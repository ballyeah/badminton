/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100129
Source Host           : localhost:3306
Source Database       : badminton

Target Server Type    : MYSQL
Target Server Version : 100129
File Encoding         : 65001

Date: 2018-09-07 16:36:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for date_closes
-- ----------------------------
DROP TABLE IF EXISTS `date_closes`;
CREATE TABLE `date_closes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of date_closes
-- ----------------------------
INSERT INTO `date_closes` VALUES ('1', '2018-09-08', '2018-09-09', null, '2018-09-07 16:29:07', '2018-09-07 16:29:07');
