<?php

    $rents = array(
        array(
            "cort" => '1',
            "start_time" => "17:00",
            "end_time" => "18.00",
            "total_time" => 2
        ),
        array(
            "cort" => '1',
            "start_time" => "19:00",
            "end_time" => "20.00",
            "total_time" => 1
        ),
    );
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Maundy | Comming Soon Page</title>
    <meta name="viewport" content="width=device-width" />
    <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    <link href='https://fonts.googleapis.com/css?family=Lobster|Open+Sans:400,400italic,300italic,300|Raleway:300,400,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/pricing-table.css">
  </head>
  <body>
    <div class="content">
      <div class="container container-bg">
        <div class="row">
          <div class="logo text-center">
            <img src="img/logo-badminton.png" alt="logo" width="150">
            <h2></h2>
          </div>
        </div>
<!--        <div class="table-responsive">-->
<!--          <table class="table table-bordered">-->
<!--            <thead>-->
<!--              <tr>-->
<!--                <th class="col-lg-1 text-center">ช่วงเวลา</th>-->
<!--                --><?php //for($i=1;$i<9;$i++) { ?>
<!--                    <th class="text-center">สนาม --><?php //echo $i; ?><!--</th>-->
<!--                --><?php //} ?>
<!--              </tr>-->
<!--            </thead>-->
<!--            <tbody>-->
<!--                --><?php //for($i=12;$i<23;$i++){ ?>
<!--                <tr>-->
<!--                    <td class="text-center">--><?php //echo $i; ?><!--:00 - --><?php //echo $i+1 ?><!--:00</td>-->
<!--                    --><?php //for($j=1;$j<9;$j++) { ?>
<!--                        <td class="col-lg-1" style="padding:5px;">-->
<!--                            --><?php //foreach($rents as $rent) {?>
<!--                                --><?php //if($rent['cort']==$j && $rent['start_time'] == $i.":00") { ?>
<!--                                    <span class="rent-1x">-->
<!---->
<!--                                    </span>-->
<!--                                --><?php //} ?>
<!--                            --><?php //} ?>
<!--                        </td>-->
<!--                    --><?php //} ?>
<!--                </tr>-->
<!--                --><?php //} ?>
<!---->
<!--            </tbody>-->
<!--          </table>-->
<!--        </div>-->
          <div class="container" style="padding-top:15px;">
              <div class="row">
                  <!-- Pricing -->
                  <?php for ($c=1;$c<=8;$c++) { ?>
                  <div class="col-md-3">
                      <div class="pricing hover-effect" style="cursor: pointer;">
                          <div class="pricing-head">
                              <img src="img/badminton-cord-<?php echo $c; ?>.png" width="100" alt="" style="padding:10px;">
                                <h4>สนาม <?php echo $c; ?></h4>
                          </div>
                          <ul class="pricing-content list-unstyled">
                              <?php for($t=12;$t<23;$t++){ ?>
                                  <li>
                                     <?php echo $t; ?>:00 - <?php echo $t+1 ?>:00
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                     <?php foreach($rents as $rent) {?>
                                          <?php if($rent['cort']==$c && $rent['start_time'] == $t.":00") { ?>
                                             <label class="label label-danger btn-xs">
                                                 <i class="fa fa-warning"></i> ไม่ว่าง
                                             </label>
                                          <?php } ?>
                                      <?php } ?>
                                  </li>
                              <?php } ?>
                          </ul>
                          <div class="pricing-footer">
                              <a data-toggle="modal" data-target="#myModalBottom">
                                <img src="img/badminton-button.png" width="150" alt="">
                              </a>
                          </div>
                      </div>
                  </div>
                  <?php } ?>
              </div>
          </div>
      </div>
    </div>

    <div class="modal fade" id="myModalBottom" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-bottom">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>ฟอร์มการจองสนาม</h2>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer" style="background-color:#2fc46e;border: #2fc46e;">
                    <button type="button" class="btn btn-warning">จอง</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                </div>
            </div>
        </div>
    </div>

    <div class="box-rent" style="">
      <p>Copyright @ 2017</p>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/custom.js"></script>
    <script>
    </script>
  </body>
</html>