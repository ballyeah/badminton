/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100129
Source Host           : localhost:3306
Source Database       : badminton

Target Server Type    : MYSQL
Target Server Version : 100129
File Encoding         : 65001

Date: 2018-09-07 16:36:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cort_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `open_time` time NOT NULL,
  `close_time` time NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `date_daily_close` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('1', 'ชยาธิป', 'null', '8', '10:00:00', '23:00:00', 'หยุดวันพุธ', 'Sun', null, null);
