<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('settings')->truncate();

        DB::table('settings')->insert([
            'name' => 'ชยาธิป',
            'logo' => 'null',
            'cort_amount' => 8,
            'open_time' => '10:00',
            'close_time' => '23:00',
            'date_daily_close' => 'Sun',
        ]);
    }
}
