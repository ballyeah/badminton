<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        DB::table('users')->truncate();

        DB::table('users')->insert([
            'username' => 'admin',
            'password' => bcrypt('jayathip'),
            'firstname' => 'ผู้ดูแลสนาม',
            'lastname' => 'ชยาธิป',
            'telephone' => '095-662-2391',
            'email' => ''
        ]);

    }
}
