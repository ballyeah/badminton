<?php

use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        DB::table('banks')->truncate();

        DB::table('banks')->insert([
           'code'=>'879-25478-9',
           'bank_name'=>'ธนาคารกรุงเทพ',
           'account_name'=>'นาย ... ...',
           'branch_name'=>'สาขา'
        ]);

        DB::table('banks')->insert([
            'code'=>'879-25478-9',
            'bank_name'=>'ธนาคารไทยพาณิชย์',
            'account_name'=>'นาย ... ...',
            'branch_name'=>'สาขา'
        ]);

        DB::table('banks')->insert([
            'code'=>'879-25478-9',
            'bank_name'=>'ธนาคารกรุงไทย',
            'account_name'=>'นาย ... ...',
            'branch_name'=>'สาขา'
        ]);

    }
}
