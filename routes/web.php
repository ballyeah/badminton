<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/rents/create/{cord_id}/{rent_date}','RentController@create')->name('rents.create');
Route::post('/rents/store','RentController@store')->name('rents.store');
Route::get('/rents/show/{id}','RentController@show')->name('rents.show');
Route::post('/rents/search','RentController@search')->name('rents.search');
Route::get('/rents/edit/{id}','RentController@edit')->name('rents.edit');
Route::put('/rents/update/{id}','RentController@update')->name('rents.update');
Route::get('/rents/delete/{id}','RentController@delete')->name('rents.delete');
Route::get('/rents/lists','RentController@lists')->name('rents.lists');
Route::post('/rents/pre','RentController@pre')->name('rents.pre');

Route::post('/payments/update-payment-cash','PaymentController@updatePaymentCash')->name('payments.update-cash');
Route::post('/payments/update-payment-deposit','PaymentController@updatePaymentDeposit')->name('payments.update-deposit');



Route::group(['middleware'=>['auth']],function() {

    Route::get('/dashboard','DashboardController@index')->name('dashboard');

    Route::get('/manage-rents','ManageRentController@index')->name('manage-rents');
    Route::get('/manage-rents/detail/{rent_id}','ManageRentController@detail')->name('manage-rents.detail');
    Route::get('/manage-rents/approve/{id}/{status}','ManageRentController@approve')->name('manage-rents.approve');
    Route::post('/manage-rents','ManageRentController@search')->name('manage-rents.search');
    Route::get('/manage-rents/delete/{id}','RentController@delete')->name('manage-rents.delete');

    Route::get('/settings','SettingController@index')->name('settings');
    Route::post('/settings','SettingController@update')->name('settings.update');

    Route::post('/settings/date-close','SettingController@storeDateClose')->name('settings.store-date-close');
    Route::get('/settings/date-close/delete/{date_close_id}','SettingController@deleteDateClose')->name('settings.delete-date-close');

    Route::post('/settings/announcement','SettingController@announcement')->name('settings.store-announcement');

    Route::get('/banks','BankController@index')->name('banks.index');
    Route::post('/banks','BankController@store')->name('banks.store');
    Route::post('/banks/update','BankController@update')->name('banks.update');
    Route::get('/banks/delete/{bank_id}','BankController@delete')->name('banks.delete');


});