@php

$setting = \App\Helpers\BadmintonHelper::webSetting();

$exOpen = explode(':', $setting->open_time);

$open = $exOpen[0];

$exClose = explode(':', $setting->close_time);

$close = $exClose[0];

\App\Helpers\BadmintonHelper::checkExpire();

@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>{{ $setting->name }} | จองสนามแบดมินตันออนไลน์</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> --}}
    <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
    <meta name="keywords"
        content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    <link
        href='https://fonts.googleapis.com/css?family=Lobster|Open+Sans:400,400italic,300italic,300|Raleway:300,400,600'
        rel='stylesheet' type='text/css'>

    @include('includes.css');

    @yield('css');

</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top"
        style="margin-bottom:200px;background-color:#0095d7;color:white;border-color: #0095d7;">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="/">
                    <img src="/maundy/img/logo-badminton.png" width="100" style="position:absolute;cursor: pointer;">
                </a>
            </div>

            <ul class="nav navbar-nav" style="cursor: pointer;">
                <li style="margin-left:120px;">
                    <a style="font-size:18px;">
                        <span id="Date"></span>&nbsp;
                        <span id="hours">{{ date('H') }}</span>:
                        <span id="min">{{ date('i') }}</span>:
                        <span id="sec">{{ date('s') }}</span>
                    </a>
                </li>
            </ul>
            <form class="navbar-form navbar-right" method="post" id="formSearchCode"
                action="{{ route('rents.search') }}">
                <div class="input-group">
                    <input type="text" class="form-control" id="code" name="code"
                        placeholder="ค้นหารายการที่จองไว้ จากเบอร์โทรศัพท์หรือรหัสจอง"
                        data-inputmask="'mask': '9999999999'" style="width:350px;">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>

            <ul class="nav navbar-nav navbar-right">

                @if (Auth::guest())
                    <li>
                        {{-- @if (Request::is('rents/pre')) --}}
                        {{-- <a href="/"> --}}
                        {{-- <i class="fa fa-clock-o"></i>&nbsp;กลับไปวันปัจจุบัน --}}
                        {{-- </a> --}}
                        {{-- @else --}}
                        <a href="#" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-clock-o"></i>&nbsp;จองสนามล่วงหน้า
                        </a>
                        {{-- @endif --}}
                    </li>
                    <li>
                        <a href="/login">
                            <i class="fa fa-sign-in"></i>&nbsp;&nbsp;เข้าสู่ระบบ
                        </a>
                    </li>
                @else
                    <li>
                        <a href="#" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-dashboard"></i>&nbsp;จองสนามล่วงหน้า
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user"></i>&nbsp;ผู้ดูแลสนาม
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="/manage-rents">
                                    <i class="fa fa-list-ul"></i>&nbsp;จัดการข้อมูล
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}" class="dropdown-toggle" data-toggle="dropdown"
                                    role="button" aria-expanded="false"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i>&nbsp;&nbsp;ออกจากระบบ
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>

                    </li>


                @endif
            </ul>


        </div>
    </nav>

    <div class="content" style="margin-top:50px;">
        <div class="container container-bg">
            @if (session()->has('message'))
                <div class="alert alert-success" id="displayMsg">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger" id="displayMsg">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><i class="fa fa-warning"></i>&nbsp;&nbsp;{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (isset($setting->content))
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <i class="fa fa-tag"></i>
                        ประกาศจากระบบ
                    </div>
                    <div class="panel-body">
                        {{-- {{App\Helpers\BadmintonHelper::checkClose()}} --}}
                        {{ $setting->content }}
                    </div>
                </div>
            @endif

            @yield('content')

        </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">จองสนามล่วงหน้า</h4>
                </div>
                <form action="{{ Route('rents.pre') }}" method="post">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="">เลือกวันที่</label>
                                <input type="text" class="form-control" name="date" id="date">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    @include('includes.javascript')

    @yield('script')


    {!! JsValidator::formRequest('App\Http\Requests\SearchCodeRequest', '#formSearchCode') !!}

    <script>
        $(document).ready(function() {
            $(":input").inputmask();
            $('#date').inputmask('99/99/9999');
            $('#date').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                language: 'th',
                startDate: '+1d'
            });
        });
    </script>

</body>

</html>
