@extends('layouts.maundy')

@section('content')


    @include('includes.menu-dashboard')
    <div class="container" style="background-color:white;width:100%;margin-top:-20px;">
        <hr>
        <form action="{{ Route('manage-rents.search') }}" method="post" class="form-inline">
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="">เลือกวันที่</label>
                        <input type="text" class="form-control" id="date" name="rent_date" value="{{ date('d/m/Y') }}">
                        <button class="btn btn-primary">
                            <i class="fa fa-search"></i>&nbsp;
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>วันที่จอง</th>
                        <th>สนาม</th>
                        <th>ช่วงเวลาที่จอง</th>
                        <th>รหัส</th>
                        <th>เบอร์โทรศัพท์</th>
                        <th>ชื่อผู้จอง</th>
                        <th>สถานะ</th>
                        <th>การชำระเงิน</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rents as $rent)
                        <tr>
                            <td>{{ Carbon\Carbon::parse($rent->rent_date)->format('d/m/Y') }}</td>
                            <td>
                                {{ $rent->cord_id }}
                            </td>
                            <td>
                                {{ Carbon\Carbon::parse($rent->start_time)->format('H:i') }}
                                -
                                {{ Carbon\Carbon::parse($rent->end_time)->format('H:i') }}
                            </td>
                            <td>{{ $rent->code }}</td>
                            <td>{{ $rent->telephone }}</td>
                            <td>
                                {{ $rent->firstname }}&nbsp;
                                {{ $rent->lastname }}
                            </td>
                            <td>
                                @if ($rent->status == 0)
                                    <label for="" class="label label-default">
                                        เลือกการชำระเงิน
                                    </label>
                                @elseif($rent->status == 1)
                                    <label for="" class="label label-warning">
                                        รอยืนยันการชำระเงิน
                                    </label>
                                @elseif($rent->status == 2)
                                    <label for="" class="label label-success">
                                        จองสำเร็จ
                                    </label>
                                @endif
                            </td>
                            <td>
                                @if (isset($rent->payment))
                                    <span>
                                        @if ($rent->payment->type == 1)
                                            <label for="" class="label label-warning">
                                                โอนเงินผ่านบัญชีธนาคาร
                                            </label>
                                        @elseif($rent->payment->type == 2)
                                            <label for="" class="label label-danger">
                                                จ่ายสดผ่านเคาเตอร์
                                            </label>
                                        @endif
                                    </span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ Route('manage-rents.detail', $rent->id) }}" class="btn btn-primary btn-xs">
                                    <i class="fa fa-info"></i>&nbsp;ดูรายละเอียด
                                </a>
                                @if ($rent->status == 1)
                                    <a onclick="deleteRent({{ $rent->id }})" class="btn btn-danger btn-xs">
                                        <i class="fa fa-times"></i>&nbsp;ยกเลิก
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('script')

    <script>
        $(function() {

            // $('table').dataTable();


        });

        function deleteRent(rentId) {

            var txt;
            var r = confirm("ยืนยันการลบ ?");
            if (r == true) {
                $.get('/manage-rents/delete/' + rentId, function(r) {
                    location.reload();
                })
            } else {}

        }
    </script>

@endsection
