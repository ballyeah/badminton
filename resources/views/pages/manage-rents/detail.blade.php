@extends('layouts.maundy')

@section('content')

    @include('includes.menu-dashboard')

    <div class="container" style="background-color:white;width: 150%;">
        <div class="row" style="background-color: #0095d7;color:white;margin-top:-20px;">
            {{-- <div class="pull-left"> --}}
            {{-- <a href="{{route('manage-rents')}}" class="btn btn-warning btn-xs"> --}}
            {{-- <i class="fa fa-caret-left"></i>&nbsp;&nbsp;รายการใบจอง --}}
            {{-- </a> --}}
            {{-- </div> --}}
            <div class="text-center">
                <h2>
                    รหัสการจอง
                    <code>
                        {{ $rent->code }}
                    </code>
                </h2>
            </div>
            <div class="pull-right">
                <a href="{{ route('manage-rents.approve', ['id' => $rent->id, 'status' => '2']) }}"
                    class="btn btn-success btn-xs">
                    <i class="fa fa-check"></i>&nbsp;&nbsp;อนุมัติ
                </a>
                <a href="{{ route('manage-rents.approve', ['id' => $rent->id, 'status' => '1']) }}"
                    class="btn btn-warning btn-xs">
                    <i class="fa fa-times"></i>&nbsp;&nbsp;ไม่อนุมัติ
                </a>
            </div>
            <hr>
        </div>
        <h2>
            <i class="fa fa-info"></i>
            ข้อมูลการจอง

        </h2>
        <span class="pull-right">
            สถานะ

            @if ($rent->status == 0)
                <label for="" class="label label-default">
                    เลือกการชำระเงิน
                </label>
            @elseif($rent->status == 1)
                <label for="" class="label label-warning">
                    รอยืนยันการชำระเงิน
                </label>
            @elseif($rent->status == 2)
                <label for="" class="label label-warning">
                    จองสำเร็จ
                </label>
            @endif

        </span>
        <hr>
        <div style="background-color: #2ecc71;color:white;padding:20px;border-radius: 5px;margin:20px;">
            <div class="container">
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>ชื่อผู้จอง</b></h4>
                    </div>
                </div>
                <div class="col-lg-9">
                    <h4>
                        คุณ {{ $rent->firstname }} {{ $rent->lastname }}
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>วันที่จอง</b></h4>
                    </div>
                </div>
                <div class="col-lg-9">
                    <h4>
                        {{ Carbon\Carbon::parse($rent->rent_date)->format('d/m/Y') }}
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>เวลาที่เริ่ม</b></h4>
                    </div>
                </div>
                <div class="col-lg-9 ">
                    <h4>
                        {{ Carbon\Carbon::parse($rent->start_time)->format('H:i') }}
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>เวลาเลิก</b></h4>
                    </div>
                </div>
                <div class="col-lg-9 ">
                    <h4>
                        {{ Carbon\Carbon::parse($rent->end_time)->format('H:i') }}
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>จำนวนเงินรวม</b></h4>
                    </div>
                </div>
                <div class="col-lg-9 ">
                    <h4>
                        {{ App\Helpers\BadmintonHelper::hourCut($rent->end_time) - \App\Helpers\BadmintonHelper::hourCut($rent->start_time) }}
                        ชั่วโมง
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>จำนวนเงินรวม</b></h4>
                    </div>
                </div>
                <div class="col-lg-9 ">
                    <h4>
                        {{ number_format((App\Helpers\BadmintonHelper::hourCut($rent->end_time) - \App\Helpers\BadmintonHelper::hourCut($rent->start_time)) * 150, 2) }}
                        บาท
                    </h4>
                </div>
            </div>
        </div>
        <h2>
            <i class="fa fa-dollar"></i>
            ข้อมูลกาารชำระเงิน
        </h2>
        @if (isset($rent->payment))
            <span class="pull-right">
                สถานะ
                @if ($rent->payment->type == 1)
                    <label for="" class="label label-warning">
                        โอนเงินผ่านบัญชีธนาคาร
                    </label>
                @elseif($rent->payment->type == 2)
                    <label for="" class="label label-danger">
                        จ่ายสดผ่านเคาเตอร์
                    </label>
                @endif
            </span>
        @else
            <h3 class="text-center">ยังไม่มีข้อมูลการชำระเงิน</h3>
        @endif
        <hr>
        @if (isset($rent->payment))
            @if ($rent->payment->type == 1)
                <div class="row">
                    <div class="col-lg-6">
                        <div class="well-lg">
                            @if (isset($rent->payment))
                                {{-- <p><b>บัญชีธนาคารที่โอน</b> : --}}
                                {{-- {{$rent->payment->bank->bank_name}} --}}
                                {{-- </p> --}}
                                <p><b>วันที่โอน</b> :
                                    @if (isset($rent->payment))
                                        {{ $rent->payment->payment_date }}
                                </p>
                            @endif
                            <p><b>เวลาที่โอน</b> :
                                {{ $rent->payment->payment_time }}
                            </p>
                            <p>
                                <b>จำนวนเงินที่โอน</b> :
                                {{ number_format($rent->payment->price, 2) }}
                            </p>
            @endif
    </div>
    </div>
    <div class="col-lg-6">
        <b>เอกสารที่แนบมา</b> :
        <img src="{{ asset('/uploads/' . $rent->payment->payment_img . '') }}" class="img-responsive" alt="">
    </div>
    </div>
@elseif($rent->payment->type == 2)
    <div class="row">
        <div class="col-lg-6">
            <div class="well-lg">
                จ่ายสดผ่านเค้าเตอร์ที่สนาม
            </div>
        </div>
    </div>
    @endif
    @endif
    <br>
    <br>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background-color: #0095d7;color:white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ฟอร์มการแจ้งการชำระเงิน</h4>
                </div>
                <form action="{{ Route('payments.update-deposit') }}" method="post" id="formUpdatePayment"
                    enctype="multipart/form-data">
                    <input type="hidden" name="rent_id" value="{{ $rent->id }}">
                    <input type="hidden" name="type" value="1">
                    <input type="hidden" name="price"
                        value="{{ (\App\Helpers\BadmintonHelper::hourCut($rent->end_time) - \App\Helpers\BadmintonHelper::hourCut($rent->start_time)) * 150 }}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">ธนาคารที่โอน</label>
                            <select name="bank_id" id="" class="form-control">
                                <option value="">- เลือก -</option>
                                <option value="1">ธนาคารกรุงเทพ</option>
                                <option value="2">ธนาคารไทยพาณิชย์</option>
                                <option value="2">ธนาคารกรุงไทย</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">วันที่ชำระเงิน</label>
                            <input type="text" class="form-control" id="inputDate" name="payment_date">
                        </div>
                        <div class="form-group">
                            <label for="">เวลาโดยประมาณ</label>
                            <input type="text" class="form-control" id="inputTime" name="payment_time">
                        </div>

                        <div class="form-group">
                            <label for="">จำนวนเงิน</label>
                            <input type="text" class="form-control" name="pay_total">
                        </div>

                        <div class="form-group">
                            <label for="">อัพโหลดหลักฐานการชำระเงิน</label>
                            <input type="file" name="payment_img">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">
                            บันทึก
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            ยกเลิก
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>

@endsection

@section('script')
    {!! JsValidator::formRequest('App\Http\Requests\PaymentRequest', '#formUpdatePayment') !!}

    <script>
        $(document).ready(function() {

            var date = new Date().getDate();

            var month = new Date().getMonth();

            var year = new Date().getYear();

            var hour = new Date().getHours();

            var min = new Date().getMinutes();

            $("#inputDate").inputmask('99/99/9999', {
                "placeholder": "dd/mm/yyyy"
            });

            $("#inputTime").inputmask('99:99', {
                "placeholder": "99:99"
            });
        });
    </script>
@endsection
