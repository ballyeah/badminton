@extends('layouts.maundy')

@section('content')

    <div class="container" style="background-color:white;width: 80%;">
        <form action="{{route('rents.update',$rent->id)}}" id="formAddRent" method="POST">
            {{ method_field('PUT') }}
            <input type="hidden" id="rent_date" name="rent_date" value="{{date('Y-m-d')}}">
            <input type="hidden" id="cord_id" name="cord_id" value="{{$cord_id}}">
            <input type="hidden" id="code" name="code" value="{{$rent->code}}">
            <div class="row text-center" >
                <img src="/maundy/img/badminton-cord-{{$cord_id}}.png" width="150" alt="" style="margin:10px;">
                <div style="background-color:darkorange;color:white;padding:2px;">
                    <h2 >
                        แก้ไขรหัสจองที่ <code>{{$rent->code}}</code>
                    </h2>
                </div>
            </div>
            <h3>
                <i class="fa fa-user"></i>
                &nbsp;
                ระบุข้อมูลผู้จอง
            </h3>
            <hr>
            <div style="background-color: #2ecc71;color:white;padding:20px;border-radius: 5px;">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">ชื่อ</label>
                            <input type="text" class="form-control" id="firstname" name="firstname" value="{{$rent->firstname}}"/>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">นามสกุล</label>
                            <input type="text" class="form-control" id="lastname" name="lastname" value="{{$rent->lastname}}"/>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="">อีเมล์</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{$rent->email}}"/>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="telephone" name="telephone" value="{{$rent->telephone}}" data-inputmask="'mask': '9999999999'"/>
                        </div>
                    </div>
                </div>
            </div>
            <h3>
                <i class="fa fa-clock-o"></i>
                &nbsp;
                เลือกช่วงเวลาการจอง

            </h3>
            <hr style="color:#2ecc71;">
            <h2>

            </h2>
            <div style="background-color: #2ecc71;color:white;padding:20px;border-radius: 5px;">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="col-lg-3 text-center">ช่วงเวลา</th>
                        <th class="text-center">สถานะ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for($j=$openTime['open'];$j<$openTime['close'];$j++){ ?>
                    <tr>
                        <td class="text-center">
                            <?php echo $j; ?>:00 - <?php echo $j+1 ?>:00
                        </td>
                        <td class="text-center">
                            @foreach($rent_cords as $cord)
                                @if($cord['start_time'] <= $j && $cord['end_time'] >= ($j+1))
                                    <i class="fa fa-times" style="color:red;"></i>
                                @endif
                            @endforeach
                            <?Php 
                                for($i=0;$i<10;$i++) {

                                    echo $i;

                                }
                            ?>

                            @for($i=0;$i<10;$i++) 

                            @endfor

                            @if($i = 1) 

                            @elseif($i = 2)

                            @else

                            @endif
                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>

                <div class="row" >
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">
                                        ช่วงเวลาเริ่ม
                                    </label>
                                    <select name="start_time" class="form-control" id="start_time">
                                        <option value="" selected> - เลือก - </option>
                                        @for($j=$openTime['open'];$j<=$openTime['close'];$j++)
                                            <option value="{{$j}}:00:00"
                                                    @foreach($rent_cords as $cord)
                                                    @if($cord['start_time'] <= $j && $cord['end_time'] >= ($j+1))
                                                    disabled style="color:red;"
                                                    @endif
                                                    @endforeach
                                                    @if($rent->start_time == ($j).':00:00')
                                                        selected
                                                    @endif>
                                                {{$j}}:00
                                                @foreach($rent_cords as $cord)
                                                    @if($cord['start_time'] <= $j && $cord['end_time'] >= ($j+1))
                                                        มีคนจองแล้ว
                                                    @endif
                                                @endforeach
                                            </option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="">
                                        ช่วงเวลาเลิก
                                    </label>
                                    <select name="end_time" class="form-control" id="end_time">
                                        <option value="" selected> - เลือก - </option>
                                        @for($j=$openTime['open'];$j<=$openTime['close'];$j++)

                                            <option value="{{($j+1)}}:00:00"
                                                    @foreach($rent_cords as $cord)
                                                        @if($cord['start_time'] <= ($j) && $cord['end_time'] >= ($j+1))
                                                        disabled style="color:red;"
                                                        @endif
                                                    @endforeach
                                                    @if($rent->end_time == ($j+1).':00:00')
                                                        selected
                                                    @endif
                                                    >
                                                {{$j+1}}:00
                                                @foreach($rent_cords as $cord)
                                                    @if($cord['start_time'] <= ($j) && $cord['end_time'] >= ($j+1))
                                                        มีคนจองแล้ว
                                                    @endif
                                                @endforeach
                                            </option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="pull-right">
                <a href="/" class="btn btn-warning">
                    <i class="fa fa-caret-left"></i>&nbsp;
                    เลือกคอร์ดใหม่
                </a>
                <button class="btn btn-primary">
                    <i class="fa fa-caret-right"></i>&nbsp;
                    จองสนาม
                </button>
            </div>
            <br>
            <br>
            <br>
        </form>
    </div>

@endsection

@section('script')

    {!! JsValidator::formRequest('App\Http\Requests\RentRequest','#formAddRent') !!}

    <script>

        $(document).ready(function(){
            $(":input").inputmask();
        });

    </script>

@endsection