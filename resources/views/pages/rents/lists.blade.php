@extends('layouts.maundy')

@section('content')

    <div class="container" style="background-color:white;width: 100%;">
        <h2><i class="fa fa-list-alt"></i>&nbsp;&nbsp;รายการที่ได้จอง</h2>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>รหัส</th>
                        <th>ชื่อผู้จอง</th>
                        <th>วันที่</th>
                        <th>เวลา</th>
                        <th>สถานะ</th>
                        <th>จัดการ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rents as $rent)
                        @if ($rent->status == 0)
                            @php $tr_class = 'active' @endphp
                        @elseif($rent->status == 1)
                            @php $tr_class = 'warning' @endphp
                        @elseif($rent->status == 2)
                            @php $tr_class = 'success' @endphp
                        @endif
                        <tr class="{{ $tr_class }}">
                            <td>{{ $rent->code }}</td>
                            <td>{{ $rent->firstname }}</td>
                            <td>{{ Carbon\Carbon::parse($rent->rent_date)->format('d/m/Y') }}</td>
                            <td>
                                {{ Carbon\Carbon::parse($rent->start_time)->format('H:i') }}
                                -
                                {{ Carbon\Carbon::parse($rent->end_time)->format('H:i') }}
                            </td>
                            <td>
                                @if ($rent->status == 0)
                                    <label for="" class="label label-default">
                                        เลือกการชำระเงิน
                                    </label>
                                @elseif($rent->status == 1)
                                    <label for="" class="label label-warning">
                                        รอยืนยันการชำระเงิน
                                    </label>
                                @elseif($rent->status == 2)
                                    <label for="" class="label label-success">
                                        จองสำเร็จ
                                    </label>
                                @endif
                            </td>
                            <td>
                                <a href="{{ Route('rents.show', $rent->id) }}" class="btn btn-primary btn-xs">
                                    ดูรายละเอียด
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('script')

@endsection
