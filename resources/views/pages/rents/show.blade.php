@extends('layouts.maundy')

@section('content')

    <div class="container" style="background-color:white;width: 80%;">
        <div class="row" style="background-color: #0095d7;color:white;padding:2px;">
            <div class="text-center">
                <h2>
                    รหัสการจอง
                    <code>
                        {{ $rent->code }}
                    </code>
                </h2>
            </div>
            <div class="pull-right">
                {{-- <a href="{{route('rents.edit',$rent->id)}}" class="btn btn-warning btn-xs"> --}}
                {{-- <i class="fa fa-pencil"></i>&nbsp;&nbsp;แก้ไข --}}
                {{-- </a> --}}
                @if ($rent->status != 2)
                    <a onclick="fncDelete({{ $rent->id }})" class="btn btn-danger btn-xs">
                        <i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก
                    </a>
                @endif
            </div>
            <hr>
        </div>
        <h2>
            <i class="fa fa-info"></i>
            ข้อมูลการจอง

        </h2>
        <span class="pull-right">
            สถานะ

            @if ($rent->status == 0)
                <label for="" class="label label-default">
                    เลือกการชำระเงิน
                </label>
            @elseif($rent->status == 1)
                <label for="" class="label label-warning">
                    รอยืนยันการชำระเงิน
                </label>
            @elseif($rent->status == 2)
                <label for="" class="label label-warning">
                    จองสำเร็จ
                </label>
            @endif

        </span>
        <hr>
        <div style="background-color: #2ecc71;color:white;padding:20px;border-radius: 5px;margin:20px;">
            <div class="container">

                <div class="col-lg-3">
                    <div class="">
                        <h4><b>ผู้จอง</b></h4>
                    </div>
                </div>
                <div class="col-lg-9">
                    <h4>
                        {{ $rent->firstname }} {{ $rent->lastname }}
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>เบอร์โทรศัพทติดต่อ</b></h4>
                    </div>
                </div>
                <div class="col-lg-9">
                    <h4>
                        {{ $rent->telephone }}
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>วันที่จอง</b></h4>
                    </div>
                </div>
                <div class="col-lg-9">
                    <h4>
                        {{ Carbon\Carbon::parse($rent->rent_date)->format('d/m/Y') }}
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>เวลาที่เริ่ม</b></h4>
                    </div>
                </div>
                <div class="col-lg-9 ">
                    <h4>
                        {{ Carbon\Carbon::parse($rent->start_time)->format('H:i') }}
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>เวลาเลิก</b></h4>
                    </div>
                </div>
                <div class="col-lg-9 ">
                    <h4>
                        {{ Carbon\Carbon::parse($rent->end_time)->format('H:i') }}
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>จำนวนเงินรวม</b></h4>
                    </div>
                </div>
                <div class="col-lg-9 ">
                    <h4>
                        {{ App\Helpers\BadmintonHelper::hourCut($rent->end_time) - \App\Helpers\BadmintonHelper::hourCut($rent->start_time) }}
                        ชั่วโมง
                    </h4>
                </div>
                <div class="col-lg-3">
                    <div class="">
                        <h4><b>จำนวนเงินรวม</b></h4>
                    </div>
                </div>
                <div class="col-lg-9 ">
                    <h4>
                        {{ number_format((\App\Helpers\BadmintonHelper::hourCut($rent->end_time) - \App\Helpers\BadmintonHelper::hourCut($rent->start_time)) * 150, 2) }}
                        บาท
                    </h4>
                </div>
            </div>
        </div>
        <h2>
            <i class="fa fa-dollar"></i>
            ช่องทางการชำระเงิน
        </h2>
        @if (isset($rent->payment))
            <span class="pull-right">
                สถานะ
                @if ($rent->payment->type == 1)
                    <label for="" class="label label-warning">
                        โอนเงินผ่านบัญชีธนาคาร
                    </label>
                @elseif($rent->payment->type == 2)
                    <label for="" class="label label-danger">
                        จ่ายสดผ่านเคาเตอร์
                    </label>
                @endif
            </span>
        @endif
        <hr>
        <div class="row">
            @if (strtotime(date('Y-m-d H:i:s')) <= strtotime($rent->expire_date))
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="card-blue" style="max-height: 550px;">
                        <div class="pull-right">
                            กำหนดเวลาที่ต้องชำระ <span id="clock" style="font-size: 25px">{{ $totalDuration }}</span>
                            เวลาที่เหลือ : <span id="clock" style="font-size: 25px;">{{ $totalDuration }}</span>
                        </div>
                        <br>
                        <h4>
                            <img src="/images/bank-2.png" width="50">
                            โอนเงินผ่านบัญชีธนาคาร
                        </h4>
                        <hr>
                        <u>เงื่อนไข</u> ชำระก่อน 30 นาที. ที่เวลากำหนดไว้ให้ ชำระผ่าน ธนาคารที่กำหนดไว้ หรือ พร้อมเพย์
                        ชำระผ่านพร้อมเพย์ค่าบริการสนามชั่วโมงละ <span style="font-size: 25px;">150</span> บาท

                        <br>

                        <br>
                        <span class="text-center" style="font-size: 30px;">
                            {{-- <p>พร้อมเพย์ 095-662-2391 (MRS. APNIYA AIMAMORNPAN)</p> --}}
                            <p><b>ธนาคารไทยพาณิชย์</b></p>
                            <p><b>บัญชีธนาคาร</b> : 433-1-32411-3</p>
                            <p><b>ชื่อบัญชี </b> : อภิญญา เอี่ยมอมรพันธ์</p>
                            <p><b>สาขา </b> : สาขา เซ็นทรัล ขอนแก่น</p>
                        </span>
                        {{-- <img src="/images/badminton-payment.jpg" class="img-responsive" alt="" width="600" style="display: block; margin: 0 auto;"> --}}
                        {{-- <img src="/images/bank-b.jpg" width="25" alt=""> --}}
                        {{-- @foreach ($banks as $bank) --}}
                        {{-- <p>{{$bank->bank_name}} {{$bank->code}} </p> --}}
                        {{-- @endforeach --}}

                        <div class="pull-right">
                            @if (isset($rent->payment))
                                @if ($rent->payment->type == 1)
                                    <button type="button" class="btn btn-success " data-toggle="modal"
                                        data-target="#myModal2">
                                        <i class="fa fa-clock-o"></i> &nbsp;ตรวจสอบการแจ้งการชำระเงิน
                                    </button>
                                @endif
                            @endif
                            <button type="button" class="btn btn-success " data-toggle="modal" data-target="#myModal1">
                                <i class="fa fa-hand-o-up"></i> &nbsp; &nbsp;แจ้งการชำระเงิน
                            </button>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-lg-6"> --}}
                {{-- <div class="card-blue"> --}}
                {{-- <h4> --}}
                {{-- <img src="/images/money.png" width="50">&nbsp;จ่ายสดผ่านทางเคาเตอร์ (จ่ายที่สนาม) --}}
                {{-- </h4> --}}
                {{-- <hr> --}}
                {{-- <p><u>เงื่อนไข</u> กรณีจ่ายผ่านเค้าเตอร์ จองแล้วสะดวกจ่ายสนามจะคิดราคา <span style="font-size: 25px;">150</span> บาท </p> --}}
                {{-- <div class="pull-right"> --}}
                {{-- <form action="{{Route('payments.update-cash')}}" --}}
                {{-- method="post"> --}}
                {{-- <input type="hidden" name="rent_id" value="{{$rent->id}}"> --}}
                {{-- <input type="hidden" --}}
                {{-- class="form-control" --}}
                {{-- name="type" --}}
                {{-- value="2"> --}}
                {{-- <input type="hidden" --}}
                {{-- class="form-control" --}}
                {{-- name="price" --}}
                {{-- value="{{(\App\Helpers\BadmintonHelper::hourCut($rent->end_time) - \App\Helpers\BadmintonHelper::hourCut($rent->start_time)) *100}}"> --}}
                {{-- <button class="btn btn-success" type="submit" --}}
                {{-- @if (isset($rent->payment)) --}}
                {{-- @if ($rent->payment->type == 2) --}}
                {{-- disabled --}}
                {{-- @endif --}}
                {{-- @endif> --}}
                {{-- <i class="fa fa-hand-o-up"></i> --}}
                {{-- &nbsp; &nbsp;จ่ายเงินสดผ่านเคาเตอร์ --}}
                {{-- </button> --}}
                {{-- </form> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- </div> --}}
            @endif
            {{-- <div class="col-lg-6"> --}}
            {{-- <div class="card-blue"> --}}
            {{-- <h4> --}}
            {{-- <img src="/images/money.png" width="50">&nbsp;จ่ายสดผ่านทางเคาเตอร์ (จ่ายที่สนาม) --}}
            {{-- </h4> --}}
            {{-- <hr> --}}
            {{-- <p><u>เงื่อนไข</u> กรณีจ่ายผ่านเค้าเตอร์ จองแล้วสะดวกจ่ายสนามจะคิดราคา <span style="font-size: 25px;">150</span> บาท </p> --}}
            {{-- <div class="pull-right"> --}}
            {{-- <form action="{{Route('payments.update-cash')}}" --}}
            {{-- method="post"> --}}
            {{-- <input type="hidden" name="rent_id" value="{{$rent->id}}"> --}}
            {{-- <input type="hidden" --}}
            {{-- class="form-control" --}}
            {{-- name="type" --}}
            {{-- value="2"> --}}
            {{-- <input type="hidden" --}}
            {{-- class="form-control" --}}
            {{-- name="price" --}}
            {{-- value="{{(\App\Helpers\BadmintonHelper::hourCut($rent->end_time) - \App\Helpers\BadmintonHelper::hourCut($rent->start_time)) *100}}"> --}}
            {{-- <button class="btn btn-success" type="submit" --}}
            {{-- @if (isset($rent->payment)) --}}
            {{-- @if ($rent->payment->type == 2) --}}
            {{-- disabled --}}
            {{-- @endif --}}
            {{-- @endif> --}}
            {{-- <i class="fa fa-hand-o-up"></i> --}}
            {{-- &nbsp; &nbsp;จ่ายเงินสดผ่านเคาเตอร์ --}}
            {{-- </button> --}}
            {{-- </form> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            <br>
            <br>
            <br>
            <div class="pull-right">


                <p class="text-warning" style="text-align: left;margin-right:50px;">
                    <u>หมายเหตุ</u> : กรณีต้องการเพิ่มชั่วโมงให้แจ้งที่เค้าเตอร์
                    และชำระเงินทันทีไม่งั้นจะปิดไฟเมื่อครบชั่วโมงที่จอง
                </p>
            </div>
        </div>
        <br>
        <br>
        <br>
    </div>

    <!-- Modal -->
    <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ตรวจสอบการแจ้งการชำระเงิน</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @if (isset($rent->payment))
                            @if ($rent->payment->type == 1)
                                <div class="col-lg-12">
                                    <div class="well-lg">
                                        {{-- <p><b>บัญชีธนาคารที่โอน</b> : --}}
                                        {{-- {{$rent->payment->bank->bank_name}} --}}
                                        {{-- </p> --}}
                                        {{-- <p><b>วันที่โอน</b> : --}}
                                        {{-- {{$rent->payment->payment_date}} --}}
                                        {{-- </p> --}}
                                        {{-- <p><b>เวลาที่โอน</b> : --}}
                                        {{-- {{$rent->payment->payment_time}} --}}
                                        {{-- </p> --}}
                                        {{-- <p> --}}
                                        {{-- <b>จำนวนเงินที่โอน</b> : --}}
                                        {{-- {{number_format($rent->payment->price,2)}} --}}
                                        {{-- </p> --}}
                                        <p><b>เอกสารที่แนบมา</b></p>

                                        <img src="{{ asset('/uploads/' . $rent->payment->payment_img . '') }}"
                                            class="img-responsive" alt="">
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background-color: #0095d7;color:white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ฟอร์มการแจ้งการชำระเงิน</h4>
                </div>
                <form action="{{ Route('payments.update-deposit') }}" method="post" id="formUpdatePayment"
                    enctype="multipart/form-data">
                    <input type="hidden" name="rent_id" value="{{ $rent->id }}">
                    <input type="hidden" name="type" value="1">
                    <input type="hidden" name="price"
                        value="{{ (\App\Helpers\BadmintonHelper::hourCut($rent->end_time) - \App\Helpers\BadmintonHelper::hourCut($rent->start_time)) * 150 }}">
                    <div class="modal-body">
                        {{-- <div class="form-group"> --}}
                        {{-- <label for="">ธนาคารที่โอน</label> --}}
                        {{-- <select name="bank_id" id="" class="form-control"> --}}
                        {{-- <option value="">- เลือก -</option> --}}
                        {{-- @foreach ($banks as $bank) --}}
                        {{-- <option value="{{$bank->id}}">{{$bank->bank_name}}</option> --}}
                        {{-- @endforeach --}}
                        {{-- </select> --}}
                        {{-- </div> --}}
                        <div class="form-group">
                            <label for="">วันที่ชำระเงิน</label>
                            <input type="text" class="form-control" id="inputDate" name="payment_date"
                                data-inputmask="'mask': '99/99/9999'">
                        </div>
                        <div class="form-group">
                            <label for="">เวลาโดยประมาณ</label>
                            <input type="text" class="form-control" id="inputTime" name="payment_time"
                                data-inputmask="'mask': '99:99'">
                        </div>

                        <div class="form-group">
                            <label for="">จำนวนเงิน</label>
                            <input type="text" class="form-control" name="pay_total">
                        </div>

                        <div class="form-group">
                            <label for="">อัพโหลดหลักฐานการชำระเงิน</label>
                            <input type="file" name="payment_img">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">
                            บันทึก
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            ยกเลิก
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>

@endsection

@section('script')
    {!! JsValidator::formRequest('App\Http\Requests\PaymentRequest', '#formUpdatePayment') !!}

    <script>
        $(document).ready(function() {


            var countDownDate = new Date('{{ $finishTime }}');

            // Update the count down every 1 second
            var x = window.setInterval(function() {
                // Get todays date and time
                var now = new Date().getTime();

                // Find the distance between now an the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Display the result in the element with id="demo"
                document.getElementById("clock").innerHTML = hours + "h " +
                    minutes + "m " + seconds + "s ";

                // If the count down is finished, write some text
                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("clock").innerHTML = "EXPIRED";
                }
            }, 1000);


            var date = new Date().getDate();

            var month = new Date().getMonth();

            var year = new Date().getYear();

            var hour = new Date().getHours();

            var min = new Date().getMinutes();

            $("#inputDate").inputmask('99/99/9999', {
                "placeholder": "dd/mm/yyyy"
            });

            $("#inputTime").inputmask('99:99', {
                "placeholder": "99:99"
            });
        });



        function fncDelete(id) {

            var r = confirm("ถ้าคุณยกเลิก 'ข้อมูลการชำระเงิน' จะถูกยกเลิกด้วย ยืนยัน ?");
            if (r == true) {

                $.get('/banks/delete/' + id, function(r) {

                    console.log('completed');

                });

                location.href = '/';

            } else {

            }

        }
    </script>
@endsection
