@extends('layouts.maundy')

@section('content')

    @include('includes.menu-dashboard')

    <div class="container" style="background-color:white;width:100%;margin-top:-20px;">
        <div class="row">
            <div class="col-lg-6">
                <h3>ตั้งค่าทั่วไป</h3>
                <hr>
                <form class="form-horizontal" action="{{Route('settings.update')}}" method="post">
                    <div class="form-group">
                        <label for="inputName3" class="col-sm-3 control-label">ชื่อสนาม</label>
                        <div class="col-sm-4">
                            <input type="text"
                                   class="form-control"
                                   id="inputName3"
                                   placeholder="ชื่อสนาม"
                                   name="name"
                                   value="{{$setting->name}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">เวลาเปิด</label>
                        <div class="col-sm-4">
                            <input type="text"
                                   class="form-control"
                                   id="inputPassword3"
                                   placeholder="เวลาเปิด"
                                   name="open_time"
                                   value="{{$setting->open_time}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">เวลาปิด</label>
                        <div class="col-sm-4">
                            <input type="text"
                                   class="form-control"
                                   id="inputPassword3"
                                   placeholder="เวลาปิด"
                                   name="close_time"
                                   value="{{$setting->close_time}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">วันหยุดประจำสัปดาห์</label>
                        <div class="col-sm-4">
                            <select class="form-control"
                                   id="inputPassword3"
                                   placeholder=""
                                   name="date_daily_close">
                                <option value="">เลือก</option>
                                <option value="Sun" @if($setting->date_daily_close == 'Sun') selected @endif>วันอาทิตย์</option>
                                <option value="Mon" @if($setting->date_daily_close == 'Mon') selected @endif>วันจันทร์</option>
                                <option value="Tue" @if($setting->date_daily_close == 'Tue') selected @endif>วันอังคาร</option>
                                <option value="Wed" @if($setting->date_daily_close == 'Wed') selected @endif>วันพุธ</option>
                                <option value="Thu" @if($setting->date_daily_close == 'Thu') selected @endif>วันพฤหัสบดี</option>
                                <option value="Fri" @if($setting->date_daily_close == 'Fri') selected @endif>วันศุกร์</option>
                                <option value="Sat" @if($setting->date_daily_close == 'Sat') selected @endif>วันเสาร์</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">ข้อความประกาศ</label>
                        <div class="col-sm-8">
                            <textarea name="content" id="content" class="form-control" cols="30" rows="10">
                                {{$setting->content}}
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-save"></i>&nbsp;&nbsp;
                                บันทึก
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-6">
                <h3>วันที่ปิดสนาม</h3>
                <hr>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ตั้งแต่วันที่</th>
                                <th>ถึงวันที่</th>
                                <th>หมายเหตุ</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($date_closes as $key_close=>$close)
                            <tr>
                                <td>{{$key_close+1}}</td>
                                <td align="center">{{$close->start_date}}</td>
                                <td align="center">{{$close->end_date}}</td>
                                <td>{{$close->remark}}</td>
                                <td>
                                    <a href="{{route('settings.delete-date-close',$close->id)}}" class="btn btn-danger">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <form class="form-horizontal" action="{{Route('settings.store-date-close')}}" id="dateClose" method="post">
                    <div class="form-group">
                        <label for="inputName3" class="col-sm-2 control-label">ตั้งแต่วันที่</label>
                        <div class="col-sm-4">
                            <input type="text"
                                   class="form-control"
                                   placeholder="dd/mm/YYYY"
                                   name="start_date"
                                   id="start_date"
                                   value=""
                                   readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">ถึงวันที่</label>
                        <div class="col-sm-4">
                            <input type="text"
                                   class="form-control"
                                   placeholder="dd/mm/YYYY"
                                   name="end_date"
                                   id="end_date"
                                   value=""
                                   readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">หมายเหตุ</label>
                        <div class="col-sm-10">
                            <input type="text"
                                   class="form-control"
                                   id="inputPassword3"
                                   placeholder="หมายเหตุ ..."
                                   name="open_time"
                                   value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-plus"></i>&nbsp;&nbsp;
                                เพิ่ม
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')

    {!! JsValidator::formRequest('App\Http\Requests\DateCloseRequest','#dateClose') !!}
    <script>


        var now = new Date();
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

        var day = days[ now.getDay() ];
        var month = months[ now.getMonth() ];
        $(document).ready(function(){

            console.log(day);

            $(":input").inputmask();
            $('#start_date').inputmask('99/99/9999');
            $('#start_date').datepicker({
                format: 'dd/mm/yyyy',
                autoclose:true,
                language: 'th',
                // startDate: '+1d'
            });

            $('#end_date').inputmask('99/99/9999');
            $('#end_date').datepicker({
                format: 'dd/mm/yyyy',
                autoclose:true,
                language: 'th',
                // startDate: '+1d'
            });
        });

    </script>
@endsection