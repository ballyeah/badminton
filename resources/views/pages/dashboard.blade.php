@extends('layouts.maundy')

@section('content')

    @include('includes.menu-dashboard')

    <div class="container" style="background-color:white;width:100%;margin-top:-20px;">
        <hr>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>สนาม</th>
                    <td>ช่วงเวลาการเล่น</td>
                </tr>
            </thead>
            <tbody>
                @for ($c = 1; $c <= $setting->cort_amount; $c++)
                    <tr>
                        <td class="col-lg-1">
                            {{ $c }}
                        </td>
                        <td>
                            @foreach ($rent_cords as $rent)
                                @if ($rent->cord_id == $c && date('H') >= \App\Helpers\BadmintonHelper::hourCut($rent['start_time']) && date('H') < \App\Helpers\BadmintonHelper::hourCut($rent['end_time']))

                                    <label for="" class="label label-success" onclick="showDetail({{ $rent->id }})">
                                        <i class="fa fa-play"></i>&nbsp;
                                        {{ $rent->start_time }} -
                                        {{ $rent->end_time }}
                                        <span id="rentDetail_{{ $rent->id }}">
                                            รหัสจองที่ {{ $rent->code }}
                                            คุณ {{ $rent->firstname }}
                                        </span>
                                    </label>&nbsp;&nbsp;
                                @endif
                            @endforeach
                        </td>
                    </tr>

                @endfor
            </tbody>
        </table>
    </div>

@endsection

@section('script')
    <script>
        function showDetail(id) {

            $('#rentDetail_' + id).toggle();

        }
    </script>
@endsection
