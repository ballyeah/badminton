@extends('layouts.maundy')

@section('content')

    {{-- <div class="container" style="background-color:white;width: 100%;padding:15px;border-radius: 5px;"> --}}

    {{-- </div> --}}
    <div class="row">
        <div class="col-lg-3">
            <h3></h3>
        </div>
    </div>
    @if (\App\Helpers\BadmintonHelper::checkClose() == 0)
        <div class="" style="padding-top:15px;">
            <div class="row">
                <!-- Pricing -->
                <?php for ($c=1;$c<=$setting->cort_amount;$c++) { ?>
                <div class="col-md-3">
                    <div class="pricing hover-effect" style="cursor: pointer;">
                        <div class="pricing-head">
                            <img src="/maundy/img/badminton-cord-<?php echo $c; ?>.png" width="100" alt=""
                                style="padding:10px;">
                            <h4>สนาม <?php echo $c; ?></h4>
                            <h3>
                                @if (isset($input))
                                    จอง ณ วันที่ {{ \App\Helpers\BadmintonHelper::dateThai($input['rent_date']) }}
                                @else
                                    จอง ณ วันที่ {{ \App\Helpers\BadmintonHelper::dateThai(date('Y-m-d')) }}
                                @endif
                            </h3>
                        </div>
                        <div style="margin:20px;">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="col-lg-3 text-center" style="width:30%;">ช่วงเวลา</th>
                                        <th class="col-lg-9 text-center" style="width:70%;">สถานะ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($t=$openTime['open'];$t<$openTime['close'];$t++){ ?>
                                    <tr>
                                        <td class="col-lg-3 text-center">
                                            <?php echo $t; ?>:00 - <?php echo $t + 1; ?>:00
                                        </td>
                                        <td class="col-lg-9 text-center">
                                            @foreach ($rents as $rent)
                                                @if (\App\Helpers\BadmintonHelper::hourCut($rent['start_time']) <= $t && \App\Helpers\BadmintonHelper::hourCut($rent['end_time']) >= $t + 1 && $rent['cord_id'] == $c)
                                                    @if ($rent->status == 1)
                                                        @php
                                                            $startTime = \Carbon\Carbon::parse(\Carbon\Carbon::now());
                                                            $finishTime = \Carbon\Carbon::parse($rent->expire_date);
                                                            $totalDuration = $finishTime->diff($startTime)->format('%H:%I:%S');
                                                        @endphp
                                                        <i class="fa fa-credit-card" style="color:coral;"></i>
                                                        รอการชำระเงินภายใน {{ $totalDuration }}
                                                    @elseif($rent->expire_date === '')
                                                        <i class="fa fa-credit-card" style="color:coral;"></i>
                                                        รอตรวจสอบการชำระเงิน
                                                    @elseif($rent->status == 2)
                                                        <i class="fa fa-check" style="color:coral;"></i>
                                                        &nbsp;คุณ&nbsp;{{ $rent->firstname }} {{ $rent->lastname }}
                                                    @endif
                                                @endif
                                            @endforeach
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="pricing-footer">
                            @if (isset($input))
                                <a
                                    href="{{ route('rents.create', ['cord_id' => $c, 'rent_date' => $input['rent_date']]) }}">
                                    <img src="/maundy/img/badminton-button.png" width="150" alt="">
                                </a>
                            @else
                                <a href="{{ route('rents.create', ['cord_id' => $c, 'rent_date' => date('Y-m-d')]) }}">
                                    <img src="/maundy/img/badminton-button.png" width="150" alt="">
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    @endif
@endsection
