@extends('layouts.maundy')


@section('content')

    @include('includes.menu-dashboard')
    <div class="container" style="background-color:white;width:100%;margin-top:-20px;">
        <div class="row">
            <div class="col-lg-8">
                <h2>บัญชีธนาคารทั้งหมด</h2>
            <hr>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:10%">ลำดับที่</th>
                            <th>ธนาคาร</th>
                            <th>เลขที่บัญชี</th>
                            <th>ชื่อบัญชี</th>
                            <th>สาขา</th>
                            <th>
                                จัดการ
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($banks as $i => $bank)
                            <form action="/banks/update" method="post" id="formUpdateBank">
                                <tr>
                                    <td valign="bottom">
                                        <span >
                                            {{$i+1}}
                                        </span>

                                        <input type="hidden"
                                               name="id"
                                               edit-input="{{$bank->id}}"
                                               id="id"
                                               value="{{$bank->id}}" style="display:none;">
                                    </td>
                                    <td valign="bottom">
                                        <span edit-span="{{$bank->id}}">
                                            {{$bank->bank_name}}
                                        </span>
                                        <select name="bank_name"
                                                id="bank_name"
                                                class="form-control"
                                                edit-input="{{$bank->id}}"
                                                style="display:none;">
                                            <option value="" >- เลือก - </option>
                                            <option value="ธนาคารกรุงไทย"
                                                    @if($bank->bank_name == 'ธนาคารกรุงไทย') selected @endif>ธนาคารกรุงไทย</option>
                                            <option value="ธนาคารกรุงเทพ"
                                                    @if($bank->bank_name == 'ธนาคารกรุงเทพ') selected @endif>ธนาคารกรุงเทพ</option>
                                            <option value="ธนาคารกสิกรไทย"
                                                    @if($bank->bank_name == 'ธนาคารกสิกรไทย') selected @endif>ธนาคารกสิกรไทย</option>
                                            <option value="ธนาคารไทยพาณิชย์"
                                                    @if($bank->bank_name == 'ธนาคารไทยพาณิชย์') selected @endif>ธนาคารไทยพาณิชย์</option>
                                            <option value="ธนาคารทหารไทย"
                                                    @if($bank->bank_name == 'ธนาคารทหารไทย') selected @endif>ธนาคารทหารไทย</option>
                                            <option value="ธนาคารกรุงศรีอยุธยา"
                                                    @if($bank->bank_name == 'ธนาคารกรุงศรีอยุธยา') selected @endif>ธนาคารกรุงศรีอยุธยา</option>
                                        </select>
                                    </td>
                                    <td valign="bottom">
                                        <span edit-span="{{$bank->id}}">
                                            {{$bank->code}}
                                        </span>
                                        <input type="text"
                                               class="form-control"
                                               name="code"
                                               edit-input="{{$bank->id}}"
                                               id="code"
                                               value="{{$bank->code}}" style="display:none;">
                                    </td>
                                    <td valign="bottom">
                                        <span edit-span="{{$bank->id}}">
                                            {{$bank->account_name}}
                                        </span>
                                        <input type="text"
                                               class="form-control"
                                               name="account_name"
                                               edit-input="{{$bank->id}}"
                                               id="account_name"
                                               value="{{$bank->account_name}}" style="display:none;">
                                    </td>
                                    <td valign="bottom">
                                        <span edit-span="{{$bank->id}}">
                                            {{$bank->branch_name}}
                                        </span>
                                        <input type="text"
                                               class="form-control"
                                               name="branch_name"
                                               edit-input="{{$bank->id}}"
                                               id="branch_name"
                                               value="{{$bank->branch_name}}" style="display:none;">
                                    </td>
                                    <td style="width:10%;">
                                        <div id="manage_{{$bank->id}}">
                                            <button type="button"
                                                    class="btn btn-warning btn-sm"
                                                    onclick="editable({{$bank->id}})">
                                                แก้ไข
                                            </button>
                                            <button type="button"
                                                    class="btn btn-danger btn-sm"
                                                    onclick="fncDelete({{$bank->id}})">
                                                ลบ
                                            </button>
                                        </div>
                                        <div id="edit_{{$bank->id}}" style="display:none;">
                                            <button type="submit"
                                                    class="btn btn-success btn-sm"
                                                    onclick="updateTable({{$bank->id}})">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button type="button"
                                                    class="btn btn-danger btn-sm"
                                                    onclick="cancelTable({{$bank->id}})">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </form>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-lg-4">
                <h2>เพิ่มบัญชีธนาคาร</h2>
                <hr>
                <form action="/banks" method="post" id="formAddBank" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="bank_name">ธนาคาร</label>
                        <div class="col-sm-8">
                            <select name="bank_name" id="bank_name" class="form-control">
                                <option value="">- เลือก - </option>
                                <option value="ธนาคารกรุงไทย">ธนาคารกรุงไทย</option>
                                <option value="ธนาคารกรุงเทพ">ธนาคารกรุงเทพ</option>
                                <option value="ธนาคารกสิกรไทย">ธนาคารกสิกรไทย</option>
                                <option value="ธนาคารไทยพาณิชย์">ธนาคารไทยพาณิชย์</option>
                                <option value="ธนาคารทหารไทย">ธนาคารทหารไทย</option>
                                <option value="ธนาคารกรุงศรีอยุธยา">ธนาคารกรุงศรีอยุธยา</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="code">เลขที่บัญชี</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="code" id="code">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="account_name">ชื่อบัญชี</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="account_name" id="account_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="account_name">สาขา</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="branch_name" id="branch_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

@section('script')

    {!! JsValidator::formRequest('App\Http\Requests\BankRequest','#formAddBank') !!}

    <script>

        function editable(bank_id) {

            $('#manage_'+bank_id+'').hide();

            $('#edit_'+bank_id+'').show();

            $('[edit-span='+bank_id+']').hide();

            $('[edit-input='+bank_id+']').show();

        }

        function updateTable (bank_id) {


            $('#manage_'+bank_id+'').show();

            $('#edit_'+bank_id+'').hide();

            $('[edit-span='+bank_id+']').show();

            $('[edit-input='+bank_id+']').hide();




        }

        function cancelTable (bank_id) {

            $('#manage_'+bank_id+'').show();

            $('#edit_'+bank_id+'').hide();

            $('[edit-span='+bank_id+']').show();

            $('[edit-input='+bank_id+']').hide();

        }

        function fncDelete(bank_id) {

            var r = confirm("ยืนยัน ?");

            if (r == true) {

                $.get('/banks/delete/'+bank_id,function(r){

                    console.log(r);

                });

                location.href = '/banks';

            } else {

            }

        }

    </script>

@endsection