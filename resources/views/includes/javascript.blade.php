<script src="/maundy/js/jquery.min.js"></script>
<script src="/maundy/js/bootstrap.min.js"></script>
<script src="/maundy/js/jquery.countdown.min.js"></script>
<script src="/maundy/js/wow.js"></script>
<script src="/maundy/js/custom.js"></script>
<script src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

{{--input mask--}}

<script src="/libs/inputmask/dist/jquery.inputmask.bundle.js"></script>
<script src="/libs/inputmask/dist/inputmask/phone-codes/phone.js"></script>
<script src="/libs/inputmask/dist/inputmask/phone-codes/phone-be.js"></script>
<script src="/libs/inputmask/dist/inputmask/phone-codes/phone-ru.js"></script>


{{--Datetime Picker--}}

<script src="/libs/datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="/libs/datetimepicker/js/locales/bootstrap-datetimepicker.th.js"></script>

{{--Date Picker--}}
<script src="/libs/datepicker/js/bootstrap-datepicker.js"></script>
<script src="/libs/datepicker/js/locales/bootstrap-datepicker.th.js"></script>

{{--Bootstrap Table--}}
<script src="/libs/bootstrap-table/dist/bootstrap-table.js"></script>
<script src="/libs/bootstrap-table/dist/bootstrap-table-locale-all.js"></script>

<script src="/vendor/datatables/datatable.min.js"></script>

{{--jQuery CountDown--}}
{{--<script src="/vendor/jquery-countdown/dist/jquery.countdown.js"></script>--}}
<script src="/vendor/jquery.countdown.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    // Making 2 variable month and day
    var monthNames = [ "มกราคม", "กุมภาพัน", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" ]; 
    var dayNames= ["อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์"]

    // make single object
    var newDate = new Date();
    // make current time
    newDate.setDate(newDate.getDate());
    // setting date and time
    //dayNames[newDate.getDay()] + " " + 
    $('#Date').html(newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

setInterval( function() {
        // Create a newDate() object and extract the seconds of the current time on the visitor's
        var seconds = new Date().getSeconds();
        // Add a leading zero to seconds value
        $("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
        },1000);

        setInterval( function() {
        // Create a newDate() object and extract the minutes of the current time on the visitor's
        var minutes = new Date().getMinutes();
        // Add a leading zero to the minutes value
        $("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
        },1000);

        setInterval( function() {
        // Create a newDate() object and extract the hours of the current time on the visitor's
        var hours = new Date().getHours();
        // Add a leading zero to the hours value
        $("#hours").html(( hours < 10 ? "0" : "" ) + hours);
    }, 1000); 
});
</script>
