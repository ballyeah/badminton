<nav class="navbar navbar-default " id="dashboard" style="background-color:#0095d7;border-color:#0095d7;border-radius: 0px;">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li class="{{ (Request::is('manage-rents') ? 'active' : '')  }}">
                <a href="/manage-rents">
                    <i class="fa fa-file-text-o"></i>&nbsp;&nbsp;จัดการข้อมูลการจอง
                </a>
            </li>
            <li class="{{ (Request::is('settings') ? 'active' : '')  }}">
                <a href="/settings">
                    <i class="fa fa-cog"></i>&nbsp;&nbsp;ตั้งค่าสนาม
                </a>
            </li>
            <li class="{{ (Request::is('banks') ? 'active' : '')  }}">
                <a href="/banks">
                    <i class="fa fa-bank"></i>&nbsp;&nbsp;บัญชีธนาคาร
                </a>
            </li>
        </ul>
    </div>
</nav>