
<link rel="stylesheet" type="text/css" href="/maundy/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/maundy/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/maundy/css/animate.css">
<link rel="stylesheet" type="text/css" href="/maundy/css/style.css">
<link rel="stylesheet" type="text/css" href="/maundy/css/pricing-table.css">

<link rel="stylesheet" type="text/css" href="/libs/datetimepicker/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" type="text/css" href="/libs/datepicker/dist/css/bootstrap-datepicker.css">
<link rel="stylesheet" type="text/css" href="/libs/bootstrap-table/dist/bootstrap-table.css">
<link href="/vendor/datatables/datatable.min.css">
