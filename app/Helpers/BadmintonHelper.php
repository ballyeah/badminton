<?php
    namespace App\Helpers;

    use App\Models\DateClose;
    use App\Models\Rent;
    use App\Models\Setting;
    use Carbon\Carbon;
    use DB;

    class BadmintonHelper {

        public static  function webSetting () {

            $result = Setting::first();

            return $result;

        }

        public static function checkClose() {

            $date = Carbon::now();

            $date = date('Y-m-d',strtotime($date));

            $date_closes = DateClose::all();

            $closed = 0;

            foreach($date_closes as $close) {

                $start_date = date('Y-m-d',strtotime($close->start_date));
                $end_date = date('Y-m-d',strtotime($close->end_date));

                if(($date >= $start_date) && ($date <= $end_date)) {

                    $closed = 1;

                }
            }

            if($closed == 0) {

                $setting = Setting::first();

                $date = Carbon::now()->format('D');

                if($setting->date_daily_close == $date) {

                    $closed = 1;

                } else {

                }

            }

            return $closed;

        }

        public static function checkExpire() {

            $rents = Rent::where('status','=',1)->whereDate('created_at', DB::raw('CURDATE()'))->get();

            foreach($rents as $rent) {
                if($rent->expire_date !== null  && $rent->expire_date <= date('Y-m-d H:i:s')) {
                    $rent->delete();
                }

            }

        }

        public static function dateThai($strDate)
        {


            $strYear = date("Y",strtotime($strDate));

            $strMonth= date("n",strtotime($strDate));

            $strDay= date("j",strtotime($strDate));

            $strHour= date("H",strtotime($strDate));

            $strMinute= date("i",strtotime($strDate));

            $strSeconds= date("s",strtotime($strDate));

            $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");

            $strMonthThai=$strMonthCut[$strMonth];

            return "$strDay $strMonthThai $strYear";

        }

        public static function openTime($open_time,$close_time) {

            $exOpen = explode(':',$open_time);
            
            $now = 8;
    
            $open = $now+1;
    
            $exClose = explode(':',$close_time);
    
            $close = $exClose[0];

            return [
                'open' => $open,
                'close' => $close,
            ];
        }

        public static function genCode() {

            return rand(1,9).rand(1,9).rand(1,9).rand(1,9);

        }

        public static  function hourCut($time) {

            $exTime = explode(':',$time);

            return $exTime[0];

        }

        public static function checkTimeDuration($cord_id,$rent_date,$start_time,$end_time,$rent_id) {

            if($rent_id != '') {

                $rent = Rent::where('id','!=',$rent_id)
                    ->whereCordId($cord_id)
                    ->whereRentDate($rent_date)
                    ->where('start_time','>=',$start_time)
                    ->where('end_time','<=',$end_time)
                    ->where('status','=',[1,2])
                    ->get();

            } else {

                $rent = Rent::whereCordId($cord_id)
                    ->whereRentDate($rent_date)
                    ->where('start_time','>=',$start_time)
                    ->where('end_time','<=',$end_time)
                    ->where('status','=',[1,2])
                    ->get();

            }

            return $rent->count();
        }

        public static function checkImageUploaded($data) {

            if(isset($data->payment_img)) {

                if (file_exists(public_path()).'/uploads/'.$data->payment_img) {

                    unlink(public_path().'/uploads/'.$data->payment_img);

                }

            }

        }

        public static function lineNoty ($message) {


            $link_token = 'S5WYZHnIMwXQZfzLt2ZEWcZMYPBbIbN7aOaHOkJZpgA';

            $lineapi = $link_token; // ใส่ token key ที่ได้มา
            $mms =  trim($message); // ข้อความที่ต้องการส่ง
            date_default_timezone_set("Asia/Bangkok");
            $chOne = curl_init();
            curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
            // SSL USE
            curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0);
            //POST
            curl_setopt( $chOne, CURLOPT_POST, 1);
            curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=$mms");
            curl_setopt( $chOne, CURLOPT_FOLLOWLOCATION, 1);
            $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$lineapi.'', );
            curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
            curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec( $chOne );
            //Check error
            if(curl_error($chOne))
            {
                echo 'error:' . curl_error($chOne);
            }
            else {
                $result_ = json_decode($result, true);
                echo "status : ".$result_['status']; echo "message : ". $result_['message'];
            }
            curl_close( $chOne );

        }

    }
?>