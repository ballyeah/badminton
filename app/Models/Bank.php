<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    //
    protected $fillable = [

        'code',
        'bank_name',
        'account_name',
        'branch_name'

    ];


    public function payment () {

        return $this->belongsTo(Payment::class);

    }
}
