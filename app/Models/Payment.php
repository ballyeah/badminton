<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $fillable = [
      'rent_id',
      'bank_id',
      'payment_date',
      'payment_time',
      'payment_img',
      'price',
        'type'
    ];

    public function rent () {

        return $this->belongsTo(Rent::class);

    }

    public function bank () {

        return $this->belongsTo(Bank::class);

    }
}
