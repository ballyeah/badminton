<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    //
    protected $fillable = [
//      'user_id',
        'rent_date',
        'cord_id',
        'code',
        'firstname',
        'lastname',
        'telephone',
        'email',
        'start_time',
        'end_time',
        'expire_date',
        'status'
    ];

    public function payment() {

        return $this->hasOne(Payment::class);

    }
}
