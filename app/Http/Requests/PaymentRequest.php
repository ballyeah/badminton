<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'rent_id' => 'required',
//            'bank_id' => 'required',
            'payment_date' => 'required',
            'payment_time' => 'required',
//            'payment_img' => 'required',
            'pay_total' => 'required',
        ];
    }
}
