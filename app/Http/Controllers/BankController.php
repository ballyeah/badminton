<?php

namespace App\Http\Controllers;

use App\Http\Requests\BankRequest;
use App\Models\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    //

    public function index () {

        $banks = Bank::all();

        return view('pages.banks.index',compact('banks'));

    }

    public function store (Request $request) {

        $input = $request->all();

        $bank = Bank::create($input);

        return redirect()->route('banks.index');

    }

    public function update (BankRequest $request) {

        $input = $request->all();

        $bank_find = Bank::find($input['id']);

        $bank_update = $bank_find->update($input);

        return redirect()->route('banks.index');

    }

    public function delete($id) {

        $bank_delete = Bank::find($id)->delete();

    }
}
