<?php

namespace App\Http\Controllers;

use App\Models\Rent;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ManageRentController extends Controller
{
    //

    public function index () {

        $rents = Rent::whereRentDate(date('Y-m-d'))->orderBy('created_at','desc')->get();

        return view('pages.manage-rents.index',compact('rents'));

    }

    public function search (Request $request) {

        $input = $request->all();

        if(isset($input['rent_date'])){

            $input['rent_date'] = Carbon::createFromFormat('d/m/Y',$input['rent_date'])->toDateString();

        }

        $rents = Rent::whereRentDate($input)->get();

        return view('pages.manage-rents.index',compact('rents'));

    }

    public function detail ($id) {

        $rent = Rent::find($id);

        return view('pages.manage-rents.detail',compact('rent'));

    }

    public function approve ($id,$status) {

        $rent = Rent::find($id)->update([
            'status' => $status,
            'expire_date' => null
        ]);

        return redirect()->route('manage-rents');

    }
}
