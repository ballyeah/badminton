<?php

namespace App\Http\Controllers;

use App\Models\Rent;
use App\Helpers\BadmintonHelper;
use App\Models\Setting;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = Setting::first();

        $openTime = BadmintonHelper::openTime($setting->open_time,$setting->close_time);

        $rents = Rent::whereDate('rent_date',date('Y-m-d'))
                    ->whereIn('status',[1,2])->get();

        return view('pages.index',compact('setting','openTime','rents'));
    }
}
