<?php

namespace App\Http\Controllers;

use App\Helpers\BadmintonHelper;
use App\Models\Rent;
use App\Models\Setting;
use App\Models\Bank;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RentController extends Controller
{
    //
    public function create($cord_id,$rent_date) {


        $setting = BadmintonHelper::webSetting();

        $openTime = BadmintonHelper::openTime($setting->open_time,$setting->close_time);

        $rent_cords = Rent::where('cord_id',$cord_id)
                        ->where('rent_date',$rent_date)
                        ->whereIn('status',[1,2])
                        ->get();
        
        return view('pages.rents.create',compact('cord_id','setting','openTime','rent_cords','rent_date'));

    }

    public function pre (Request $request) {

        $input = $request->all();

        $input['rent_date'] = Carbon::createFromFormat('d/m/Y',$input['date'])->toDateString();

        $setting = Setting::first();

        $openTime = BadmintonHelper::openTime($setting->open_time,$setting->close_time);

        $rents = Rent::whereDate('rent_date',$input['rent_date'])
                        ->where('status','=',2)
                        ->get();

        return view('pages.index',compact('setting','openTime','rents','input'));

    }

    public function store(Request $request) {

        $input = $request->all();

        DB::beginTransaction();

        $input['code'] = BadmintonHelper::genCode();

        $resultTimeDuration = BadmintonHelper::checkTimeDuration(
                                $input['cord_id'],
                                $input['rent_date'],
                                $input['start_time'],
                                $input['end_time'],
                                ''
                                );

        if($resultTimeDuration > 0) {
            return redirect()->back()->withErrors([
                'message'=>'ช่วงเวลาซ้ำกัน'
            ]);
        }

        $rent_create = Rent::create($input);

        $input['expire_date'] = Carbon::parse($rent_create->created_at)->addMinutes(30);

        $rent_create->update([
            'expire_date' => $input['expire_date']
        ]);


        $msg = 'คุณ '.$input['firstname'].' ได้ทำการจองสนาม ดูรายละเอียดเพิ่มเติมที่ รหัสจอง '.$input['code'].' '.route('manage-rents.detail',$rent_create->id);

        BadmintonHelper::lineNoty($msg);

        DB::commit();

        return redirect()->route('rents.show',$rent_create->id);

    }

    public function update (Request $request,$id) {

        $input = $request->all();

        $resultTimeDuration = BadmintonHelper::checkTimeDuration(
            $input['cord_id'],
            $input['rent_date'],
            $input['start_time'],
            $input['end_time'],
            $id
        );

        if($resultTimeDuration > 0) {
            return redirect()->back()->withErrors([
                'message'=>'ช่วงเวลาซ้ำกัน'
            ]);
        }

        $rent_find = Rent::find($id);

        $rent_update = $rent_find->update($input);

        return redirect()->route('rents.show',$id);

    }

    public function edit($id) {

        $setting = BadmintonHelper::webSetting();

        $openTime = BadmintonHelper::openTime($setting->open_time,$setting->close_time);

        $rent = Rent::find($id);

        $rent_cords = Rent::where('id','!=',$id)
            ->where('cord_id',$rent->cord_id)
            ->where('rent_date',date('Y-m-d'))
            ->get();

        $cord_id = $rent->cord_id;

        return view('pages.rents.edit',compact('cord_id','rent','rent_cords','setting','openTime'));

    }

    public function search(Request $request) {

        $input = $request->all();

        $code = str_replace('_','',$input['code']);

        if(strlen($code) == 4){

            $rent = Rent::whereCode($code)->first();

            if($rent === null) {

                return redirect()->back()->withErrors([
                    'message'=>'ไม่พบรหัสการจอง'
                ]);

            }

            return redirect()->route('rents.show',$rent->id);

        } else {

            $rents = Rent::whereTelephone($code)->orderBy('created_at','desc')->get();

            return view('pages.rents.lists',compact('rents'));


        }

    }

    public function lists () {

        return view('pages.rents.lists');

    }

    public function show($id) {

        $rent = Rent::find($id);
        $startTime = Carbon::parse(Carbon::now());
        $finishTime = Carbon::parse($rent->expire_date);
        $totalDuration = $finishTime->diff($startTime)->format('%H:%I:%S');
        $banks = Bank::all();

        return view('pages.rents.show',compact('rent','banks','totalDuration','finishTime'));

    }

    public function delete($id) {

        $rent = Rent::find($id)->delete();

    }
}
