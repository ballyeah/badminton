<?php

namespace App\Http\Controllers;

use App\Helpers\BadmintonHelper;
use Illuminate\Http\Request;
use App\Models\Rent;

class DashboardController extends Controller
{
    //
    public function index () {

        $setting = BadmintonHelper::webSetting();

        $openTime = BadmintonHelper::openTime($setting->open_time,$setting->close_time);

        $rent_cords = Rent::where('rent_date',date('Y-m-d'))->orderBy('start_time')->get();

        return view('pages.dashboard',compact('setting','openTime','rent_cords'));

    }
}
