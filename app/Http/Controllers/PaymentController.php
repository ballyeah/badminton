<?php

namespace App\Http\Controllers;

use App\Helpers\BadmintonHelper;
use App\Http\Requests\PaymentRequest;
use App\Models\Payment;
use App\Models\Rent;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    //
    public function updatePaymentCash (Request $request) {

        $input = $request->all();

        $payment_find = Payment::whereRentId($input['rent_id'])->first();

        $rent_update = Rent::find($input['rent_id'])
                            ->update(['status'=>'1']);

        if($payment_find) {

            //update
            $payment_update = $payment_find->update($input);

        } else {

            //create
            $payment_create = Payment::create($input);

        }

        return redirect()->route('rents.show',$input['rent_id']);

    }

    public function updatePaymentDeposit (PaymentRequest $request) {

        $input = $request->all();

        $rent = Rent::find($input['rent_id']);

        $payment_find = Payment::whereRentId($input['rent_id'])->first();

        if (isset($input['payment_img'])) {

            //upload
            if ($payment_find) {

                //update
//                BadmintonHelper::checkImageUploaded($payment_find);

                $input['payment_img'] = time().$rent->code.'.'.$request->payment_img->getClientOriginalExtension();

                $request->payment_img->move('uploads/', $input['payment_img']);

                $payment_find->update($input);


            } else {

                //create
                $input['payment_img'] = time().$rent->code.'.'.$request->payment_img->getClientOriginalExtension();

                $request->payment_img->move('uploads/', $input['payment_img']);

                Payment::create($input);

            }

        } else {

            //don't upload

            if($payment_find) {

                //update
                $input['payment_img'] = $payment_find->payment_img;

                $payment_find->update($input);

            } else {

                //create
                Payment::create($input);

            }

        }

        $rent->update([
            'expire_date' => null
        ]);

        $msg = 'คุณ '.$rent->firstname.' ได้ทำการแจ้งการชำระเงิน ดูรายละเอียดเพิ่มเติมที่ รหัสจอง '.$rent->code.' '.route('manage-rents.detail',$rent->id);

        BadmintonHelper::lineNoty($msg);

        return redirect()->route('rents.show',$input['rent_id']);
    }
}
