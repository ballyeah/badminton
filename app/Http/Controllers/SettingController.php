<?php

namespace App\Http\Controllers;

use App\Models\DateClose;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    //

    public function index () {

        $setting = Setting::first();

        $date_closes = DateClose::all();

        return view('pages.settings.index',compact('setting','date_closes'));

    }

    public function update (Request $request) {

        $input = $request->all();

        $setting_find = Setting::first()->update($input);

        return redirect()->route('settings');

    }

    private function formatDate($date) {

        $ex_date = explode('/',$date);

        return $ex_date[2].'-'.$ex_date[1].'-'.$ex_date[0];

    }

    public function storeDateClose(Request $request) {

        $input = $request->all();

        $input['start_date'] = $this->formatDate($input['start_date']);

        $input['end_date'] = $this->formatDate($input['end_date']);

        DateClose::create($input);

        return redirect()->route('settings');

    }

    public function deleteDateClose($date_close_id) {

        DateClose::find($date_close_id)->delete();

        return redirect()->route('settings');


    }
}
